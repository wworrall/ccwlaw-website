<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
    lang="en-GB" xml:lang="en-GB">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta charset="UTF-8" />
  <meta name="description" content="Commercially focused solicitors acting for
  companies and individuals. Based in Llandybie near Ammanford, Carmarthenshire,
  South Wales. We offer our clients our wealth of experience with a personal
  service and at competitive local rates."/>
  <meta name="keywords" content="CCW Law, Solicitors, Lawyers, Commercial
  Solicitors, South Wales, Carmarthenshire, Ammanford, Commercial Lawyers, Chris
  Worrall, Sonia Worrall, Worrall" />
  <meta name="robots" content="index,follow" />
  <meta name="author" content="William Worrall" />

  <title>CCW Law Solicitors - Home</title>

  <!-- Font -->
  <link href='https://fonts.googleapis.com/css?family=Muli:400,300'
  rel='stylesheet' type='text/css' />

  <!-- Styles -->
  <link rel="stylesheet" href="/css/normalize.css" type="text/css" />
  <link rel="stylesheet" href="/css/skeleton.css" type="text/css" />
  <link rel="stylesheet" href="/css/styles.css" type="text/css" />
  <link rel="icon" type="image/png" href="img/favicon.png" />

  <!-- JS Plugins -->
  <script src=
      "https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js">
  </script>
  <script src="js/jquery.flexslider-min.js"></script>

  <!-- JS -->
  <script src="/js/nav.js"></script>
  <script src="/js/myslider.js"></script>
  <script src="/js/expand-tiles.js"></script>

</head>

<body>
  <!-- Google Analytics -->
  <?php include_once("inc/analyticstracking.php") ?>

  <section class="table-container"><!--for sticky footer-->
    <section class="table-block footer-push">
      <!-- Primary Page Layout
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

      <!-- Header (logo) -->
      <?php
        include("inc/header.php");
      ?>

      <!-- Primary Navigation Bar -->
      <?php
        include("inc/nav.php");
      ?>

      <!-- Main page content -->
      <section class="band main">
        <section class="container content">

          <!-- FlexSlider w/ company images-->
          <section class="flexslider">
            <ul class="slides">
              <li>
                <figure>
                  <img src="/img/london-eye-top-image.png" alt="" />
                </figure>
              </li>
              <li>
                <figure>
                  <img src="/img/wind-turbine-top-image.png" alt="" />
                </figure>
              </li>
              <li>
                <figure>
                  <img src="/img/crain-construction-top-image.png" alt="" />
                </figure>
              </li>
            </ul>
          </section>

          <!-- About section -->
          <a name="about"></a>
          <h1>About</h1>
          <hr />
          <section class="row">
            <section class="one-half column">
              <h2>Ethos of Our Practice</h2>
              <p>
                CCW Law is a two-partner practice consisting of husband and wife
                team, Chris and Sonia Worrall.
              </p>
              <p>
                CCW Law always aims to deliver commercially focused advice
                whilst keeping clients informed as their transactions progress.
              </p>
              <p>
                We offer our clients our wealth of experience with a personal
                service and at competitive local rates. Our clients instruct us
                in the knowledge that any work is carried out by us personally,
                and not delegated to juniors.
              </p>
              <h2>Clients</h2>
              <p>
                Our Clients include limited companies, large and small,
                partnerships, insolvency practitioners and individuals.
              </p>
            </section>
            <section class="one-half column">
              <h2>London and Cardiff Experience</h2>
              <p>
                Chris and Sonia Worrall are London and Cardiff trained and have
                experience of working in national law firms and local
                government.
              </p>
              <p>
                Chris comes from a background of commercial law and served his
                articles in Lincoln’s Inn, London. Prior to establishing CCW Law
                Chris was a Partner at Morgan Cole (now Blake Morgan LLP), Foot
                Anstey LLP and Leo Abse and Cohen in Cardiff (now Slater &amp;
                Gordon) where he headed up the commercial team.
              </p>
              <p>
                Sonia’s background is in local government law having trained at
                the London Borough of Hounslow, before working in the legal
                departments of the London Borough of Ealing, Carmarthen District
                Council and The City and County of Swansea.
              </p>
            </section>
          </section>

          <!-- Services section -->
          <a name="services"></a>
          <h1>Services</h1>
          <hr />
          <p>
            Our services include:
          </p>
          <section class="row">
            <section class="one-half column">
              <section class="services">
                <ul>
                  <li>
                    <div>
                      Contract Law
                    </div>
                    <p>
                      Drafting and advising on commercial contracts and contract
                      disputes.
                    </p>
                  </li>
                  <li>
                    <div>
                      Shareholder, Director &amp; Partnership Issues
                    </div>
                    <p>
                      Acting and advising on duties of shareholders, directors
                      and partners.
                    </p>
                  </li>
                  <li>
                    <div>
                      Insolvency &amp; Bankruptcy
                    </div>
                    <p>
                      Advising on company insolvency and personal bankruptcy
                      including acting for insolvency practitioners.
                    </p>
                  </li>
                  <li>
                    <div>
                      Debt Recovery
                    </div>
                    <p>
                      Acting for businesses and individuals on the recovery of
                      bad debts.
                    </p>
                  </li>
                </ul>
              </section><!--end services pt1 of 2-->
            </section><!--end half-column-->
            <section class="one-half column">
              <section class="services">
                <ul>
                  <li>
                    <div>
                      Commercial &amp; Land Disputes
                    </div>
                    <p>
                      Acting for companies and individuals arguing over money,
                      property, and land and taking disputes to mediation.
                    </p>
                  </li>
                  <li>
                    <div>
                      Landlord &amp; Tenant
                    </div>
                    <p>
                      Drafting of leases, lease renewals, and termination.
                    </p>
                  </li>
                  <li>
                    <div>
                      Local Government Law &amp; Planning
                    </div>
                    <p>
                      Acting on all aspects of local government law and planning
                      matters.
                    </p>
                  </li>
                  <li>
                    <div>
                      Property &amp; Land Transfers
                    </div>
                    <p>
                      Acting for businesses and individuals buying and selling
                      property and land.
                    </p>
                  </li>
                </ul>
              </section><!--end services pt2 of 2-->
            </section><!--end half-column-->
          </section><!--end row-->
        </section><!--end container-->
      </section><!--end band main-->
    </section><!--end table-block footer-push-->


    <section class="table-block"><!--for sticky footer-->
      <!-- Page Footer Layout
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
      <!-- Footer -->
      <?php
        include("inc/footer.php");
      ?>
    </section><!--end table-block-->

  </section><!--end table-container-->
</body>
</html>
