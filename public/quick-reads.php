<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
    lang="en-GB" xml:lang="en-GB">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta charset="UTF-8" />
  <meta name="description" content="CCW Law Solicitors. Interesting quick-read
  articles by Chris Worrall."/>
  <meta name="keywords" content="Quick-Reads, Articles, Chris Worrall, CCW Law,
  Solicitors, Worrall" />
  <meta name="robots" content="index,follow" />
  <meta name="author" content="William Worrall" />

  <title>Quick Reads - CCW Law</title>

  <!-- Font -->
  <link href='https://fonts.googleapis.com/css?family=Muli:400,300'
  rel='stylesheet' type='text/css' />

  <!-- Styles -->
  <link rel="stylesheet" href="/css/normalize.css" type="text/css" />
  <link rel="stylesheet" href="/css/skeleton.css" type="text/css" />
  <link rel="stylesheet" href="/css/styles.css" type="text/css" />
  <link rel="icon" type="image/png" href="img/favicon.png" />

  <!-- JS Plugins -->
  <script src=
      "https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js">
  </script>

  <!-- JS -->
  <script src="/js/nav.js"></script>
  <script src="/js/expand-articles.js"></script>

</head>

<body>
  <!-- Google Analytics -->
  <?php include_once("inc/analyticstracking.php") ?>

  <section class="table-container"><!--for sticky footer-->
    <section class="table-block footer-push">
      <!-- Primary Page Layout
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

      <!-- Header (logo) -->
      <?php
        include("inc/header.php");
      ?>

      <!-- Primary Navigation Bar -->
      <?php
        include("inc/nav.php");
      ?>

      <!-- Main page content -->
      <section class="band main">
        <section class="container content">

          <!-- About section -->
          <h1>Quick Reads</h1>
          <hr />
          <p>
            Articles by Chris Worrall.
          </p>
          <article class="quick-read">
            <section class="row">
              <section class="four columns">
                <img src="/img/quick-reads/private-car.png" alt=""></img>
              </section>
              <section class="eight columns">
                <h2>Private Car Park Tickets</h2>
                <section class="article-text">
                  <p>
                    I have had a flurry of cases recently about private car
                    parking tickets, or to be more accurate from Clients whose
                    University siblings have landed them with private car
                    parking claims because my Clients were the registered keeper
                    of the vehicle.
                  </p>
                  <p>
                    One player is Link Parking Limited who manage private car
                    parks nationally and who regularly instruct a firm of
                    solicitors based in Cheshire called Gladstones.
                  </p>
                  <p>
                    My advice is to ignore the demands for payment; don't get
                    involved in any appeal process; you will not lose as it is
                    just a device to reel you in.
                  </p>
                  <p>
                    If the registered keeper is different to the driver who
                    parked the car on the day in question any claim should be
                    defended on the basis that there is no contract between the
                    car park enforcement company and the registered keeper. A
                    second line of defence is to ask to see the contract between
                    the car park enforcement company and the actual car park
                    landowner to see if the land owner has given consent to the
                    car parking company issuing court proceedings in its own
                    name for a breach of contract for trespass upon land which
                    is not owned or assigned to the car park enforcement
                    company. More often than not there is no such written
                    contract.
                  </p>
                </section><!--end article text-->
                <button class=expand></button>
              </section>
            </section>
          </article><!--end article-->

          <article class="quick-read">
            <section class="row">
              <section class="four columns">
                <img src="/img/quick-reads/assured-shorthold.png" alt=""></img>
              </section>
              <section class="eight columns">
                <h2>Assured Shorthold Tenancies &amp; Parental Guarantees</h2>
                <section class="article-text">
                  <p>
                    It is coming up to that time when university students sign
                    up to shorthold tenancy agreements for the new academic year
                    and more often than not ask Mum or Dad to sign an
                    accompanying parental guarantee in favour of the Landlord.
                    If the digs are let through a managing agent it is often
                    impossible to get out of signing such a guarantee which not
                    only covers the rent but also any breach of covenant.
                  </p>
                  <p>
                    If parents have no alternative but to sign a personal
                    guarantee they should check that under the terms of the
                    guarantee only their own child's rent and breach of covenant
                    is being guaranteed. Quite often the guarantee (which has to
                    be read in conjunction with the tenancy agreement) is
                    drafted so that each personal guarantor guarantees payment
                    of the rent not only for their own child but all the other
                    flat mates too.
                  </p>
                </section><!--end article text-->
                <button class=expand></button>
              </section>
            </section>
          </article><!--end article-->

        </section><!--end container-->
      </section><!--end band main-->
    </section><!--end table-block footer-push-->


    <section class="table-block"><!--for sticky footer-->
      <!-- Page Footer Layout
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
      <!-- Footer -->
      <?php
        include("inc/footer.php");
      ?>
    </section><!--end table-block-->

  </section><!--end table-container-->
</body>
</html>
