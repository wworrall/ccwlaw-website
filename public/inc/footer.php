<section class="band footer">
  <footer class="container">
    <p>
      Registered office: The Old Surgery, 5 Church Street, Llandybie,
      Ammanford, SA18 3HZ, United Kingdom.<br />
      &#xA9;2016 CCW Law Solicitors Limited. Registered number 05854634. A
      list of directors is available at the registered office.<br />
      Authorised and Regulated by the Solicitors Regulation Authority SRA No.
      446175
    </p>
    <p>
      Website design by William Worrall.
    </p>
  </footer><!--end container-->
</section><!--end band main-->
