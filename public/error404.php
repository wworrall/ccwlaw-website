<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
    lang="en-GB" xml:lang="en-GB">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta charset="UTF-8" />
  <meta name="description" content="Error 404. Page not found."/>
  <meta name="keywords" content="Error, 404, Page not found" />
  <meta name="robots" content="index,follow" />
  <meta name="author" content="William Worrall" />

  <title>Error 404. Page not found - CCW Law Solicitors</title>

  <!-- Font -->
  <link href='https://fonts.googleapis.com/css?family=Muli:400,300'
  rel='stylesheet' type='text/css' />

  <!-- Styles -->
  <link rel="stylesheet" href="/css/normalize.css" type="text/css" />
  <link rel="stylesheet" href="/css/skeleton.css" type="text/css" />
  <link rel="stylesheet" href="/css/styles.css" type="text/css" />
  <link rel="icon" type="image/png" href="img/favicon.png" />

  <!-- JS Plugins -->
  <script src=
      "https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js">
  </script>
  <!--note google maps api included at bottom of body due to async and
    callback-->

  <!-- JS -->
  <script src="/js/nav.js"></script>

</head>

<body>
  <!-- Google Analytics -->
  <?php include_once("inc/analyticstracking.php") ?>

  <section class="table-container"><!--for sticky footer-->
    <section class="table-block footer-push">

      <!-- Primary Page Layout
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

      <!-- Header (logo) -->
      <?php
        include("inc/header.php");
      ?>

      <!-- Primary Navigation Bar -->
      <?php
        include("inc/nav.php");
      ?>

      <!-- Main page content -->
      <section class="band main">
        <section class="container content">

          <!-- About section -->
          <h1>Error 404</h1>
          <hr />
          <h2>The page you requested could not be found. Please visit our
            <a href="/">home page</a>.
          </h2>
        </section><!--end container-->
      </section><!--end band main-->
    </section><!--end table-block footer-push-->

    <section class="table-block"><!--for sticky footer-->
      <!-- Page Footer Layout
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
      <!-- Footer -->
      <?php
        include("inc/footer.php");
      ?>
    </section><!--end table-block-->

  </section><!--end table-container-->

  <!-- Include google maps api - see JS plugins above
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjFi6Qnwjo3swdXa_hgHVQHwvMpQXicOE&callback=responsiveMap" async defer></script>

</body>
</html>
