"use strict"

// Function is called by callback once the google maps api has loaded.
function responsiveMap() {

    // location of CCW LAW
    var CCW_LAT_LNG = {lat: 51.821027, lng: -4.006070}

    // initiate map centred at CCW_LAT_LNG
    var map;
    map = new google.maps.Map(document.querySelector('#gmap'), {
        center: CCW_LAT_LNG,
        zoom: 16
    });

    // add a marker at CCW_LAT_LNG
    var marker = new google.maps.Marker({
        position: CCW_LAT_LNG,
        map: map
    });

    // deal with recentering upon window size change
    var center;
    function calculateCenter() {
        center = map.getCenter();
    }
    google.maps.event.addDomListener(map, 'idle', function() {
        calculateCenter();
    });
    google.maps.event.addDomListener(window, 'resize', function() {
        map.setCenter(center);
    });

}
