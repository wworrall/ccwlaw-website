"use strict"

// document ready used because it doesn't wait for content to load
$(document).ready(responsiveNav);

function responsiveNav() {

    // build drop down menu
    $("<select />").appendTo(".navigation nav");

    // create default option (Go to..)
    $("<option />", {
            "selected": "selected",
            "value": "",
            "text": "Go to..."
            }).appendTo("nav select");

    // populate with items from the other menu
    $(".navigation nav li a").each(function() {
        var el = $(this);
        $("<option />", {
                "value": el.attr("href"),
                "text": el.text()
                }).appendTo("nav select");
    });

    // makes responsive drop down menu load selected page
    $("nav select").change(function() {
        window.location = $(this).find("option:selected").val();
    });

}
