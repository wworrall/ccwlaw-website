"use strict"

// document ready used because it doesn't wait for content to load
$(document).ready(expandableItems);

function expandableItems() {
    // add onclick listeners
    $(".services ul li div").each(function() {
        var div = $(this);
        div.click(function() {
            openClose($(this)[0]);
        });
    });
}

function openClose(div) {
    var par = $(div).parent().children("p");

    // open if closed
    if (par.css("height") == "0px") {
        par.css({"height": "auto", "overflow": "auto",
                "opacity": "10", "filter": "alpha(opacity=100)"});
    }
    // close if open
    else {
        par.css({"height": "0px", "overflow": "none",
                "opacity": "0", "filter": "alpha(opacity=0)"});
    }
}
