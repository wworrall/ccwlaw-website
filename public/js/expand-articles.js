"use strict"

// document ready used because it doesn't wait for content to load
$(document).ready(expandableArticles);

// Edit these as required
var MORE_TEXT = "...read more";
var LESS_TEXT = "...show less";

// for saving minimised text height
var minHeight;

function expandableArticles() {

    // add onclick listeners and button inner html
    $("article button.expand").each(function() {
        var button = $(this);
        button[0].innerHTML = MORE_TEXT;
        button.click(function() {
            openClose($(this)[0]);
        });
    });
}

function openClose(button) {
    var text = $(button).parent().children(".article-text");

    // open if closed
    if (button.innerHTML == MORE_TEXT) {
        minHeight = text.css("height"); // save height
        text.css({"height": "auto", "overflow": "auto"});
        button.innerHTML = LESS_TEXT;
    }
    // close if open
    else {
        text.css({"height": minHeight, "overflow": "hidden"});
        button.innerHTML = MORE_TEXT;
    }
}
