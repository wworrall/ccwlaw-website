<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
    lang="en-GB" xml:lang="en-GB">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta charset="UTF-8" />
  <meta name="description" content="Commercially focused solicitors acting for
  companies and individuals. Contact us. Address: CCW Law Solicitors Limited,
  The Old Surgery, 5 Church Street, Llandybie, Ammanford, SA18 3HZ, United
  Kingdom. We recommend booking an appointment over email or telephone."/>
  <meta name="keywords" content="Contact, Telephone, Telephone Number, Email,
  Address, CCW Law, Solicitors, Lawyers, Commercial, South Wales,
  Carmarthenshire, Ammanford, Chris Worrall, Sonia Worrall, Worrall" />
  <meta name="robots" content="index,follow" />
  <meta name="author" content="William Worrall" />

  <title>Contact - CCW Law Solicitors</title>

  <!-- Font -->
  <link href='https://fonts.googleapis.com/css?family=Muli:400,300'
  rel='stylesheet' type='text/css' />

  <!-- Styles -->
  <link rel="stylesheet" href="/css/normalize.css" type="text/css" />
  <link rel="stylesheet" href="/css/skeleton.css" type="text/css" />
  <link rel="stylesheet" href="/css/styles.css" type="text/css" />
  <link rel="icon" type="image/png" href="img/favicon.png" />

  <!-- JS Plugins -->
  <script src=
      "https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js">
  </script>
  <!--note google maps api included at bottom of body due to async and
    callback-->

  <!-- JS -->
  <script src="/js/nav.js"></script>
  <script src="/js/res-map.js"></script>

</head>

<body>
  <!-- Google Analytics -->
  <?php include_once("inc/analyticstracking.php") ?>

  <section class="table-container"><!--for sticky footer-->
    <section class="table-block footer-push">

      <!-- Primary Page Layout
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

      <!-- Header (logo) -->
      <?php
        include("inc/header.php");
      ?>

      <!-- Primary Navigation Bar -->
      <?php
        include("inc/nav.php");
      ?>

      <!-- Main page content -->
      <section class="band main">
        <section class="container content">

          <!-- About section -->
          <h1>Contact</h1>
          <hr />
          <section class="row">
            <section class="one-half column">
              <section class="contact">
                <h2>Find us here:</h2>
                <p id="address">
                  CCW Law Solicitors Limited, The Old Surgery,<br/>
                  5 Church Street, Llandybie, Ammanford,<br/>
                  SA18 3HZ, United Kingdom.
                </p>
                <section id="gmap"></section>
                <h2>Office hours:</h2>
                <p>
                  Office hours are flexible. We recommend phoning or
                  emailing to make an appointment.
                </p>
                <h2>LinkedIn:</h2>
                <section id="linkedin">
                  <p>
                    <!--link code from
                      https://www.linkedin.com/profile/profile-badges, also
                      removed font and font-size from style attribute-->
                    <a href="https://uk.linkedin.com/pub/chris-worrall/98/581/468" style="text-decoration:none;"><span style="color:#0783B6;"><img src="https://static.licdn.com/scds/common/u/img/webpromo/btn_in_20x15.png" width="20" height="15" alt="View Chris Worrall's LinkedIn profile" style="vertical-align:middle;" border="0">&nbsp;View Chris Worrall's profile</span></a>
                  </p>
                </section>
              </section><!--end contact-->
            </section><!--end half column-->

            <section class="one-half column">
              <section class="contact">
                <h2>Contact us via:</h2>
                <ul>
                  <li>
                    <img src="/img/telephone.png" alt="" /><br/>
                    Office: +44 (0)1269 851905
                  </li>
                  <li>
                    <img src="/img/mobile.png" alt="" /><br/>
                    Chris Worrall: +44 (0)7450 724542<br/>
                    Sonia Worrall: +44 (0)7450 724543
                  </li>
                  <li>
                    <img src="/img/email.png" alt="" /><br/>
                    Email:
                    <a href="info [at] ccwlawsolicitors.co.uk"
                       rel="nofollow"
                       onclick="this.href='mailto:' + 'info' + '&#x40;' + 'ccwlawsolicitors.co.uk'">
                       info@ccwlawsolicitors.co.uk</a>
                    </a>
                  </li>
                  <li>
                    <img src="/img/fax.png" alt="" /><br/>
                    Fax: +44 (0)1269 851907
                  </li>
                </ul>
              </section><!--end contact-->
            </section><!--end half column-->
          </section><!--end row-->
        </section><!--end container-->
      </section><!--end band main-->
    </section><!--end table-block footer-push-->


    <section class="table-block"><!--for sticky footer-->
      <!-- Page Footer Layout
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
      <!-- Footer -->
      <?php
        include("inc/footer.php");
      ?>
    </section><!--end table-block-->

  </section><!--end table-container-->

  <!-- Include google maps api - see JS plugins above
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjFi6Qnwjo3swdXa_hgHVQHwvMpQXicOE&callback=responsiveMap" async defer></script>

</body>
</html>
